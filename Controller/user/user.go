package user

import (
	"encoding/json"
	config "lokapala-backend/Config"
	model "lokapala-backend/Model"
	"lokapala-backend/Utility"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

var db = config.Db

var JwtKey = []byte(Utility.GoDotEnvVariable("SECRET_KEY"))

func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}

	email := r.Form.Get("email")
	password := r.Form.Get("password")

	stmt, err := db.Query("SELECT user_id, email, password, id_role FROM user WHERE email LIKE ? ", email)

	if err != nil {
		panic(err.Error())
	}

	defer stmt.Close()

	var user model.User

	for stmt.Next() {
		err := stmt.Scan(&user.User_id, &user.Email, &user.Password, &user.Id_role)
		if err != nil {
			panic(err.Error())
		}
	}

	check := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

	if check != nil {
		json.NewEncoder(w).Encode("Wrong Password!")
		panic(check.Error())
	} else {
		// Declare the expiration time of the token
		exparationTime := time.Now().Add(60 * time.Minute)

		// Create the JWT claims, which includes the username and expiry time
		claims := model.Claims{
			User_id: user.User_id,
			Email: user.Email,
			Role_id: user.Id_role,
			StandardClaims: jwt.StandardClaims{
				// In JWT, the expiry time is expressed as unix milliseconds
				ExpiresAt: exparationTime.Unix(),
			},
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		tokenString, err := token.SignedString(JwtKey)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		jwtToken := model.Token{
			Token: tokenString,
		}

		json.NewEncoder(w).Encode(jwtToken)
	}
}

func Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err.Error())
	}

	stmt, err := db.Prepare("INSERT INTO user(name, username, email, password, city, id_role) VALUES(?,?,?,?,?,?)")
	
	if err != nil {
		panic(err.Error())
	}

	name := r.Form.Get("name")
	username := r.Form.Get("username")
	email := r.Form.Get("email")
	password := r.Form.Get("password")
	city := r.Form.Get("city")
	id_role := r.Form.Get("id_role")

	pwd := []byte(password)

	pwdhash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)

	if err != nil {
		panic(err.Error())
	}

	_, err = stmt.Exec(name, username, email, pwdhash, city, id_role)
	if err != nil {
		panic(err.Error())
	}

	defer stmt.Close()

	w.Header().Set("Content-Type", "application/json")
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	var users []model.User

	result, err := db.Query("SELECT user_id, name, username, email, city, id_role FROM user")
	if err != nil {
		panic(err.Error())
	}

	for result.Next() {
		var user model.User
		err := result.Scan(&user.User_id, &user.Name, &user.Username, &user.Email, &user.City, &user.Id_role)
		if err != nil {
			panic(err.Error())
		}

		users = append(users, user)

	}

	defer result.Close()

	json.NewEncoder(w).Encode(users)
}
