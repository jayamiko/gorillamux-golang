package role

import (
	"encoding/json"
	config "lokapala-backend/Config"
	model "lokapala-backend/Model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

var db = config.Db

// Get All Ksatriya
func GetRoles(w http.ResponseWriter, r *http.Request) {
	var roles []model.Role
	result, err := db.Query("SELECT * FROM lokapala.role")
	if err != nil {
		panic(err.Error())
	}

	for result.Next() {
		var role model.Role
		err := result.Scan(&role.Role_id, &role.Name)
		if err != nil {
			panic(err.Error())
		}
		roles = append(roles, role)
	}

	defer result.Close()
	json.NewEncoder(w).Encode(roles)
	// ResponseJson(w, http.StatusOK, ksatriyas) // Response JSON
}