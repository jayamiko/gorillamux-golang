package ksatriya

import (
	"encoding/json"
	config "lokapala-backend/Config"
	helper "lokapala-backend/Helper"
	model "lokapala-backend/Model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

var db = config.Db

var ResponseJson = helper.ResponseJson

// Get All Ksatriya
func GetKsatriyas(w http.ResponseWriter, r *http.Request) {
	var ksatriyas []model.Ksatriya
	result, err := db.Query("SELECT id, name, role_id FROM lokapala.ksatriya")
	if err != nil {
		panic(err.Error())
	}

	for result.Next() {
		var ksatriya model.Ksatriya
		err := result.Scan(&ksatriya.Id, &ksatriya.Name, &ksatriya.Role_id)
		if err != nil {
			panic(err.Error())
		}
		ksatriyas = append(ksatriyas, ksatriya)
	}

	defer result.Close()
	json.NewEncoder(w).Encode(ksatriyas)
	// ResponseJson(w, http.StatusOK, ksatriyas) // Response JSON
}