package main

import (
	"log"
	"lokapala-backend/Controller/user"
	middleware "lokapala-backend/Middleware"
	"lokapala-backend/Routers"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)



func main() {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})

	router := mux.NewRouter()
	// Route Login
	router.HandleFunc("/api/login", user.Login).Methods("POST")

	// Route Register
	router.HandleFunc("/api/register", user.Register).Methods("POST")

	route := router.PathPrefix("/api").Subrouter()

	route.Use(middleware.Middleware)
	route.Handle("/", Routers.RouteUser(route))
	route.Handle("/", Routers.RouteRole(route))
	route.Handle("/", Routers.RouteKsatriya(route))

	log.Fatal(http.ListenAndServe(":8000", handlers.CORS(originsOk, headersOk, methodsOk)(router)))
}