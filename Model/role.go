package model

type Role struct {
	Role_id int32  `json:"role_id"`
	Name    string `json:"name"`
}