package model

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	User_id int64 `json:"user_id"`
	Email string `json:"email"`
	Role_id int32 `json:"role_id"`
	jwt.StandardClaims
}