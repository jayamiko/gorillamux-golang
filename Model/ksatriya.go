package model

type Ksatriya struct {
	Id      int64  `json:"id"`
	Name    string `json:"name"`
	Role_id string `json:"role_id"`
}