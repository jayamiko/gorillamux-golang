package model

type User struct {
	User_id int64 `json:"user_id"`
	Name string `json:"name"`
	Username string `json:"username"`
	Email string `json:"email"`
	Password string `json:"password"`
	City string `json:"city"`
	Id_role int32 `json:"id_role"`
}