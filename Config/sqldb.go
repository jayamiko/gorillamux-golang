package config

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

var Db *sql.DB

func init() {
	Db = SqlOpen()
}

func goDotEnvVariable(key string) string {
	// Load .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf(("Error loading .env file"))
	}

	return os.Getenv(key)
}


func SqlOpen() (dbase *sql.DB) {

	driver := goDotEnvVariable("DB_DRIVER")
	database := goDotEnvVariable("DB_DATABASE")
	username := goDotEnvVariable("DB_USERNAME")
	password := goDotEnvVariable("DB_PASSWORD")
	address := goDotEnvVariable("DB_ADDRESS")
	port := goDotEnvVariable("DB_PORT")

	dbase, err := sql.Open(driver, username+":"+password+"@tcp("+address+":"+port+")"+"/"+database)

	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Connection Database Success")
	}

	dbase.SetConnMaxIdleTime(10 * time.Second)
	dbase.SetConnMaxLifetime(60 * time.Second)
	dbase.SetMaxOpenConns(5)
	dbase.SetMaxIdleConns(5)
	return dbase
}