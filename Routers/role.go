package Routers

import (
	"lokapala-backend/Controller/role"

	"github.com/gorilla/mux"
)

func RouteRole(r *mux.Router) *mux.Router {
	route_ksatriya := r.PathPrefix("/role").Subrouter()
	// route_ksatriya.Use(Middleware.Middleware)

	route_ksatriya.HandleFunc("/getRoles", role.GetRoles).Methods("GET")
	return r
}