package Routers

import (
	"lokapala-backend/Controller/ksatriya"

	"github.com/gorilla/mux"
)

func RouteKsatriya(r *mux.Router) *mux.Router {
	route_ksatriya := r.PathPrefix("/ksatriya").Subrouter()
	// route_ksatriya.Use(Middleware.Middleware)

	route_ksatriya.HandleFunc("/getKsatriyas", ksatriya.GetKsatriyas).Methods("GET")
	return r
}