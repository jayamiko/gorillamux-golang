package Routers

import (
	"lokapala-backend/Controller/user"
	middleware "lokapala-backend/Middleware"

	"github.com/gorilla/mux"
)

func RouteUser(r *mux.Router) *mux.Router {
	route_user := r.PathPrefix("/user").Subrouter()
	route_user.Use(middleware.Middleware, middleware.CheckPermission)

	route_user.HandleFunc("/getUsers", user.GetAllUsers).Methods("GET")
	return r
}